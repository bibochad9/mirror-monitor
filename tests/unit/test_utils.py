from fdroid_mirror_monitor import utils
from unittest import mock
import responses
import subprocess
import unittest


class Fake_subprocess:
    def __init__(self, returncode):
        self.returncode = returncode
        self.stderr = 'error: %s' % self.returncode
        self.stdout = 'stdout'


class TestMethods(unittest.TestCase):
    try:
        log = utils.get_logger(__name__, 'DEBUG')
    except RuntimeError:
        log = utils.get_logger(__name__)

    #    def setUp(self):

    def test_get_geoip_v4(self):
        result = utils.geoip('8.8.8.8')
        expected = ('United States', 'US')
        self.assertEqual(result, expected)

    def test_get_geoip_v6(self):
        result = utils.geoip('2001:4860:4860::8888')
        expected = ('United States', 'US')
        self.assertEqual(result, expected)

    @responses.activate
    def test_get_own_ip(self):
        expected = '1.2.3.4'
        responses.add(responses.GET, 'https://ident.me', status=200, body=expected)
        result = utils.get_own_ip()
        self.assertEqual(result, expected)

    @responses.activate
    def test_get_own_ip_fail(self):
        responses.add(responses.GET, 'https://ident.me', status=404)
        result = utils.get_own_ip()
        self.assertEqual(result, None)

        '''
        - needs self.url to be set
        - set self.protocol to http(s), rsync
        - set self.hostname, self.path and self.onion
        - add trailing slash for self.url
        '''

    def test_init_protocols(self):
        g = utils.Generic()
        g.url = 'https://mirror1.local/repo/'
        g.init_protocols()
        self.assertEqual(g.protocol, 'https')
        self.assertEqual(g.hostname, 'mirror1.local')
        self.assertEqual(g.path, '/repo/')
        self.assertEqual(g.onion, False)

        g = utils.Generic()
        g.url = 'rsync://mirror1.onion/repo/'
        g.init_protocols()
        self.assertEqual(g.protocol, 'rsync')
        self.assertEqual(g.hostname, 'mirror1.onion')
        self.assertEqual(g.path, '/repo/')
        self.assertEqual(g.onion, True)

        g = utils.Generic()
        g.url = 'http://mirror1.local/repo'
        g.init_protocols()
        self.assertEqual(g.protocol, 'http')
        self.assertEqual(g.path, '/repo/')

        g = utils.Generic()
        g.url = 'ftp://mirror1.local/repo'
        g.init_protocols()
        self.assertEqual(g.protocol, 'unknown')

    @mock.patch('subprocess.run', return_value=Fake_subprocess(0))
    def test_check_file_rsync_0(self, mock_run):
        g = utils.Generic()
        g.rsync_pw = None
        g.timeout = 10
        result = g.check_file('rsync://mirror.local/repo')
        self.assertEqual(result, True)

    @mock.patch('subprocess.run', return_value=Fake_subprocess(23))
    def test_check_file_rsync_23(self, mock_run):
        g = utils.Generic()
        g.rsync_pw = '123'
        g.timeout = 10
        result = g.check_file('rsync://mirror.local/repo')
        self.assertEqual(result, False)

    @mock.patch('subprocess.run', return_value=Fake_subprocess(1))
    def test_check_file_rsync_1(self, mock_run):
        g = utils.Generic()
        g.rsync_pw = None
        g.timeout = 10
        with self.assertRaises(subprocess.CalledProcessError) as context:
            g.check_file('rsync://mirror.local/repo')
        self.assertEqual(context.exception.stdout, 'stdout')
        self.assertEqual(context.exception.stderr, 'error: 1')

    def test_check_file_unknown_protocol(self):
        g = utils.Generic()
        with self.assertRaises(ValueError) as context:
            g.check_file('ftp://mirror.local/repo')
        self.assertEqual(str(context.exception), '''Can't get file with unknown protocol ftp://mirror.local/repo''')

    def test_check_file_onion(self):
        g = utils.Generic()
        with self.assertRaises(ValueError) as context:
            g.check_file('ftp://mirror.onion/repo')
        self.assertEqual(str(context.exception), 'Getting file via Tor is not supported (yet) ftp://mirror.onion/repo')

    def test_get_file_unknown_protocol(self):
        g = utils.Generic()
        with self.assertRaises(ValueError) as context:
            g.get_file('ftp://mirror.local/repo')
        self.assertEqual(str(context.exception), '''Can't get file with unknown protocol ftp://mirror.local/repo''')

    def test_get_file_onion(self):
        g = utils.Generic()
        with self.assertRaises(ValueError) as context:
            g.get_file('ftp://mirror.onion/repo')
        self.assertEqual(str(context.exception), 'Getting file via Tor is not supported (yet) ftp://mirror.onion/repo')


if __name__ == '__main__':
    unittest.main()
