import unittest
from fdroid_mirror_monitor import mirror, utils


class TestMethods(unittest.TestCase):
    url1 = 'https://mirror1.local/repo/'
    url1b = 'https://mirror1.local/repo2/'
    url2 = 'https://mirror2.local/repo/'
    url3 = 'https://mirror3.local/repo/'
    try:
        log = utils.get_logger(__name__, 'DEBUG')
    except RuntimeError:
        log = utils.get_logger(__name__)

    def setUp(self):
        mirror.Mirror.instances.clear()

    def test_init(self):
        mirror.Mirror.timeout = 10
        m1 = mirror.Mirror(self.url1, headers={'User-Agent': 'unittest'})
        m1.log = self.log

        self.assertEqual(len(mirror.Mirror.instances), 1)
        self.assertEqual(m1 in mirror.Mirror.instances, True)
        self.assertEqual(m1.url, self.url1)
        self.assertEqual(m1.index_file, 'index-v1.jar')
        self.assertEqual(m1.rsync_pw, None)
        self.assertEqual(m1.timeout, 10)
        self.assertEqual(m1.headers, {'User-Agent': 'unittest'})

        self.assertEqual(m1.hostname, 'mirror1.local')
        self.assertEqual(m1.protocol, 'https')
        self.assertEqual(m1.onion, False)
        self.assertEqual(m1.repo_fullpaths, ['/repo/'])
        self.assertEqual(m1.errors, {})

        #  create duplicate
        m2 = mirror.Mirror(self.url1, headers={'User-Agent': 'unittest'})
        m2.log = self.log

        self.assertEqual(m2, m1)

        self.assertEqual(len(mirror.Mirror.instances), 1)
        self.assertEqual(m2 in mirror.Mirror.instances, True)
        self.assertEqual(m2.url, self.url1)
        self.assertEqual(m2.index_file, 'index-v1.jar')
        self.assertEqual(m2.rsync_pw, None)
        self.assertEqual(m2.timeout, 10)
        self.assertEqual(m2.headers, {'User-Agent': 'unittest'})

        self.assertEqual(m2.hostname, 'mirror1.local')
        self.assertEqual(m2.protocol, 'https')
        self.assertEqual(m2.onion, False)
        self.assertEqual(m2.repo_fullpaths, ['/repo/'])
        self.assertEqual(m2.errors, {})

        #  create duplicate with modifications
        m3 = mirror.Mirror(self.url1, rsync_pw='lol', headers={'User-Agent': 'unittest_not_equal'})
        m3.log = self.log
        self.assertEqual(m3, m1)
        self.assertEqual(len(mirror.Mirror.instances), 1)
        self.assertEqual(m3 in mirror.Mirror.instances, True)
        self.assertEqual(m3.url, self.url1)
        self.assertEqual(m3.index_file, 'index-v1.jar')
        self.assertEqual(m3.rsync_pw, None)
        self.assertEqual(m3.timeout, 10)
        self.assertEqual(m3.headers, {'User-Agent': 'unittest'})

        self.assertEqual(m3.hostname, 'mirror1.local')
        self.assertEqual(m3.protocol, 'https')
        self.assertEqual(m3.onion, False)
        self.assertEqual(m3.repo_fullpaths, ['/repo/'])
        self.assertEqual(m3.errors, {})

        #  create duplicate with different repo
        m4 = mirror.Mirror(self.url1b)
        m4.log = self.log
        self.assertEqual(m4, m1)
        self.assertEqual(len(mirror.Mirror.instances), 1)
        self.assertEqual(m4 in mirror.Mirror.instances, True)
        self.assertEqual(m4.url, self.url1)
        self.assertEqual(m4.index_file, 'index-v1.jar')
        self.assertEqual(m4.rsync_pw, None)
        self.assertEqual(m4.timeout, 10)
        self.assertEqual(m4.headers, {'User-Agent': 'unittest'})

        self.assertEqual(m4.hostname, 'mirror1.local')
        self.assertEqual(m4.protocol, 'https')
        self.assertEqual(m4.onion, False)
        self.assertEqual(m4.repo_fullpaths, ['/repo/', '/repo2/'])
        self.assertEqual(m4.errors, {})

    def test_get_data(self):
        data_expected = {
            'url': 'https://mirror1.local/repo/',
            'index_file': 'index-v1.jar',
            'index_urls': {'https://mirror1.local/repo/index-v1.jar'},
            'rsync_pw': None,
            'errors': {},
            'hostname': 'mirror1.local',
            'protocol': 'https',
            'onion': False,
            'name': 'https:mirror1.local',
            'repo_fullpaths': ['/repo/'],
            'repo_relpaths': [],
        }

        m = mirror.Mirror(self.url1)
        m.log = self.log
        data = m.get_data()
        print(data)
        self.assertEqual(data_expected, data)
        print(data)


if __name__ == '__main__':
    unittest.main()
