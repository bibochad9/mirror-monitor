# Mirror Monitor

Monitor the status of known mirrors F-Droid Mirrors.
Live demo: https://marzzzello.gitlab.io/mirror-monitor/


## Active Mirrors of https://f-droid.org/

### repo + archive
* https://f-droid.org/
* rsync://fdroid-mirror@mirror.f-droid.org/ RSYNC_PASSWORD=dont-abuse-me-please
* https://fdroid.tetaneutral.net/fdroid/
* https://bubu1.eu/fdroid/
* https://fdroid.swedneck.xyz/fdroid/
* https://ftp.fau.de/fdroid/
* rsync://ftp.fau.de/fdroid/
* http://fauftpffbmvh3p4h.onion/fdroid/
* https://plug-mirror.rcac.purdue.edu/fdroid/
* rsync://plug-mirror.rcac.purdue.edu/fdroid/
* https://mirrors.tuna.tsinghua.edu.cn/fdroid/
* rsync://mirrors.tuna.tsinghua.edu.cn/fdroid/
* https://mirrors.nju.edu.cn/fdroid/
* rsync://mirrors.nju.edu.cn/fdroid/
* https://mirror.kumi.systems/fdroid/
* rsync://mirror.kumi.systems/fdroid/
* https://ftp.lysator.liu.se/pub/fdroid/
* rsync://ftp.lysator.liu.se/pub/fdroid/
* http://lysator7eknrfl47rlyxvgeamrv7ucefgrrlhk7rouv3sna25asetwid.onion/pub/fdroid/
* rsync://lysator7eknrfl47rlyxvgeamrv7ucefgrrlhk7rouv3sna25asetwid.onion/pub/fdroid/

### repo only
* https://mirror.cyberbits.eu/fdroid/
* rsync://mirror.cyberbits.eu/fdroid/
* https://ftp.osuosl.org/pub/fdroid/
* https://mirror.scd31.com/fdroid/
* https://fdroid.fi-do.io/fdroid/
* https://mirror.librelabucm.org/fdroid/
* rsync://mirror.librelabucm.org/fdroid/
* http://mirror.librelablmozifzc.onion/fdroid/
