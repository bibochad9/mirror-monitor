#!/usr/bin/env python3

# internal:
from fdroid_mirror_monitor import repo, mirror, history, website
from fdroid_mirror_monitor.utils import get_logger, geoip, get_own_ip, get_mirrors_readme, get_repos_wiki

# stdlibs
from datetime import datetime, timezone
from importlib.metadata import metadata
from os import makedirs, path
from queue import Queue
import argparse
import json
import threading
import traceback


def main():
    parser = argparse.ArgumentParser(prog='fdroid_mirror_monitor', description=metadata(__package__)['Summary'])
    parser.add_argument(
        '-v', '--verbosity', help='Set verbosity level', choices=['warning', 'info', 'debug'], default='info'
    )
    subparsers = parser.add_subparsers(help='Desired action to perform', dest='command')

    # help
    subparsers.add_parser('help', help='Print this help message')

    # Create parent subparser for {import, check}-parsers with common arguments
    parent_parser = argparse.ArgumentParser(add_help=False)
    parent_parser.add_argument('--html', help='create htmls', action='store_true')
    parent_parser.add_argument(
        '--trim', help='Trim history entries to the last x hours', type=int, default=168, metavar='HOURS'
    )

    # Subparsers based on parent

    # import
    parser_import = subparsers.add_parser(
        'import', parents=[parent_parser], help='Import local status.json/history.json'
    )
    parser_import.add_argument(
        'from_json', help='Use history.json or status.json', choices=['history', 'status'],
    )
    parser_import.add_argument('directory', help='Input/output directory', metavar='PATH')

    # check
    parser_check = subparsers.add_parser('check', parents=[parent_parser], help='Check repos and mirrors')
    parser_check.add_argument(
        '--history_url', help='URL to the the history in JSON format. Append report to it and save as history.json'
    )
    parser_check.add_argument(
        '-r',
        '--repo',
        help='Repo URL (optional: add additional mirror URLs with comma)',
        action='append',
        metavar='REPO_URL[,MIRROR_URL,MIRROR_URL,...]',
    )
    parser_check.add_argument(
        '--repos_wiki',
        help='Include repos from https://forum.f-droid.org/t/known-repositories/721',
        action='store_true',
    )
    parser_check.add_argument('--mirrors_readme', help='Include mirrors from README.md', action='store_true')
    parser_check.add_argument(
        '-t',
        '--threads',
        help='Number of parallel threads for checking repos and mirrors',
        choices=range(1, 31),
        metavar='[1-30]',
        type=int,
        default=10,
    )
    parser_check.add_argument('--timeout', help='Timeout for requests', type=int, default=60, metavar='SECONDS')
    parser_check.add_argument('directory', help='Output directory', metavar='PATH')

    args = parser.parse_args()
    if args.command == 'help' or args.command is None:
        parser.print_help()
        exit()
    print(vars(args))

    output_dir = path.realpath(args.directory)

    # init logger
    log = get_logger(__name__, args.verbosity)
    log.debug('logging is set to debug')

    def save_status(status):
        log.debug('Save status as %s/status.json' % output_dir)
        with open(path.join(output_dir, 'status.json'), 'w') as fp:
            json.dump(status, fp)

    def gen_html(status):
        if args.html:
            log.info('Creating HTMLs')
            website.create_html(status=status, dir=output_dir)

    if args.command == 'import' and args.from_json == 'history':
        log.info('Importing %s/history.json' % output_dir)
        with open(output_dir + '/history.json', 'r') as fp:
            h = history.History()

            # convert timestamp string to int
            h.history = json.load(
                fp, object_hook=lambda d: {int(k) if k.lstrip('-').isdigit() else k: v for k, v in d.items()}
            )
            log.debug('The history.json has %s entries', len(h.history))

        h.trim(args.trim)

        h.save(path.join(output_dir, 'history_trimmed.json'))
        log.info('Save trimmed history as %s/history_trimmed.json' % output_dir)

        status = h.historical_status(args.trim)

        save_status(status)
        gen_html(status)
        exit(0)

    if args.command == 'import' and args.from_json == 'status':
        with open(output_dir + '/status.json', 'r') as fp:
            status = json.load(fp)
        gen_html(status)
        exit(0)

    if args.command == 'check':

        # set timeout for classes
        mirror.Mirror.timeout = args.timeout
        repo.Repo.timeout = args.timeout

        # official  mirrors
        if args.mirrors_readme:
            repo_mirrors, archive_mirrors = get_mirrors_readme()

        repos = []
        if args.repo:
            for rm in args.repo:
                rm = rm.split(',')
                r = repo.Repo(rm[0])

                r_mirrors = set()
                for mirror_url in rm[1:]:
                    r_mirrors.add(mirror.Mirror(mirror_url))
                r.set_additional_mirrors(list(r_mirrors))

                repos.append(r)

        if args.repos_wiki:
            repos.extend(get_repos_wiki())
        #################################
        # run all repo tests as threads

        def worker(q, exceptions):
            while not q.empty():
                repo_obj = q.get()
                threading.currentThread().name = repo_obj.url

                if args.mirrors_readme and repo_obj.url == 'https://f-droid.org/repo/':
                    repo_obj.set_additional_mirrors(repo_mirrors)
                if args.mirrors_readme and repo_obj.url == 'https://f-droid.org/archive/':
                    repo_obj.set_additional_mirrors(archive_mirrors)

                try:
                    repo_obj.get_all()
                    q.task_done()
                except Exception as e:
                    log.fatal(e)
                    log.error(traceback.format_exc())
                    exceptions[repo.url] = e
                    q.task_done()

        # put all repos in queue
        jobs = Queue()
        for repo_obj in repos:
            jobs.put(repo_obj)

        exceptions = {}
        # start threads
        for _ in range(args.threads):
            t = threading.Thread(target=worker, args=(jobs, exceptions,), daemon=True)
            t.start()

        # wait for threads to finish
        jobs.join()

        for t in exceptions:
            log.fatal('Exception in thread %s: %s' % (t, exceptions[t]))
        if len(exceptions) > 0:
            exit(1)

        #################################
        # run all mirror tests as threads
        mirrors = mirror.Mirror.instances
        timestamp = int(datetime.now(timezone.utc).timestamp())

        def worker(q, exceptions):
            while not q.empty():
                mirror_obj = q.get()
                threading.currentThread().name = mirror_obj.name
                try:
                    mirror_obj.run_all()
                    q.task_done()
                except Exception as e:
                    log.fatal(e)
                    log.error(traceback.format_exc())
                    exceptions[mirror_obj.name] = e
                    q.task_done()

        # put all mirrors in queue
        jobs = Queue()
        for mirror_obj in mirrors:
            jobs.put(mirror_obj)

        exceptions = {}
        # start threads
        for _ in range(args.threads):
            t = threading.Thread(target=worker, args=(jobs, exceptions,), daemon=True)
            t.start()

        # wait for threads to finish
        jobs.join()

        for t in exceptions:
            log.fatal('Exception in thread %s: %s' % (t, exceptions[t]))
        if len(exceptions) > 0:
            exit(1)

        ###############
        # create status
        mirror_reports = []
        for mirror_obj in mirrors:
            mirror_reports.append(mirror_obj.get_data())

        # non_main_mirrors = []
        # for repo_obj in repos:
        #     non_main_mirrors.extend(repo_obj.integrated_mirrors)
        #     non_main_mirrors.extend(repo_obj.additional_mirrors)

        repo_reports = []
        for repo_obj in repos:
            # # check for duplicate where repo is just a known mirror
            # if repo_obj.main_mirror not in non_main_mirrors:
            repo_reports.append(repo_obj.get_data())

        status = dict()
        status['version'] = metadata(__package__)['Version']
        status['last_check'] = timestamp
        status['check_ip'] = get_own_ip()
        status['check_country'], status['check_country_code'] = geoip(status['check_ip'])
        status['mirrors'] = mirror_reports
        status['repos'] = repo_reports

        makedirs(output_dir, exist_ok=True)

        ##################################################
        # create history, add historical context to status
        h = None
        if args.history_url:
            log.info('Adding report to history')
            try:
                h = history.History(args.history_url, timeout=args.timeout)
                log.debug('The history had %s entries', len(h.history))
            except Exception as e:
                log.warning(str(e))
                log.error('Could not fetch history from %s', args.history_url)
                log.info('Creating empty history')
                h = history.History()

            h.add(status, timestamp)
            h.trim(args.trim)

            h.save(path.join(output_dir, 'history.json'))
            status = h.historical_status(args.trim)
        ##################################################

        # save status.json
        save_status(status)

        # create HTMLs
        if args.html:
            log.info('Creating HTMLs')
            website.create_html(status=status, dir=output_dir)
