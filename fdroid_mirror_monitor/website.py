#!/usr/bin/env python3

# stdlibs:
from datetime import datetime, timezone
from importlib.metadata import metadata
from os import makedirs, path, getenv
from urllib.parse import quote
from fdroid_mirror_monitor.utils import get_logger, sizeof_fmt

# external:
from jinja2 import Environment, FileSystemLoader
from json2html import json2html


def create_html(status, dir):
    '''
    Create html on current status.

    :param status: dict
    :param dir: output directory of html
    :param repo_name: headline 1
    '''
    log = get_logger(__name__)

    # generate html from template with jinja
    template_dir = path.join(path.dirname(__file__), path.pardir, 'templates')
    template_dir = path.abspath(template_dir)

    # timestamp to date string
    last_check = datetime.fromtimestamp(status['last_check'], tz=timezone.utc).strftime("%F %H:%M %Z")

    makedirs(dir, exist_ok=True)
    env = Environment(loader=FileSystemLoader(template_dir))
    env.globals.update(zip=zip, int=int, sizeof_fmt=sizeof_fmt)
    env.trim_blocks = True
    env.lstrip_blocks = True

    # render base.html
    base = getenv('CI_PAGES_URL', '/').rstrip('/') + '/'
    template = env.get_template('base.html')
    sourceurl = getenv('CI_PROJECT_URL', metadata(__package__)['Home-Page']).rstrip('/') + '/'
    index = template.render(
        base=base, sourceurl=sourceurl, commit=getenv('CI_COMMIT_SHORT_SHA', ''), joburl=getenv('CI_JOB_URL', ''),
    )
    with open(path.join(template_dir, 'base_rendered.html'), 'w') as fp:
        fp.write(index)

    # render mirror details subpages
    # detaildir = dir + '/mirrors'
    mirror_dir = path.join(dir, 'mirror')
    makedirs(mirror_dir, exist_ok=True)

    template = env.get_template('mirror.html')
    for mirror in status['mirrors']:
        table = json2html.convert(json=mirror)
        # makedirs(detaildir + mirror[name], exist_ok=True)
        with open(path.join(mirror_dir, mirror['name'] + '.html'), 'w') as f:
            f.write(template.render(mirror=mirror, table=table, ref_name=getenv('CI_COMMIT_REF_NAME')))

    # render mirror_index.html
    template = env.get_template('mirror_index.html')
    index = template.render(status=status, last_check=last_check,)
    with open(path.join(mirror_dir, 'index.html'), 'w') as fp:
        fp.write(index)

    # render repo details subpages
    # detaildir = dir + '/mirrors'index
    repo_dir = path.join(dir, 'repo')
    makedirs(repo_dir, exist_ok=True)

    template = env.get_template('repo.html')
    for repo in status['repos']:
        # makedirs(detaildir + mirror[name], exist_ok=True)
        filename = repo['url'].replace(repo['protocol'] + '://', '')
        filename = filename.rstrip('/')
        if filename.find(':') != -1:
            err = 'repo url contains ":" %s' % repo['url']
            log.error(err)
            repo['errors']['url'] = err

        filename = filename.replace('/', ':')
        repo['filename'] = filename
        try:
            repo['size_all_packages'] = sizeof_fmt(repo['size_all_packages'])
            repo['size_current_packages'] = sizeof_fmt(repo['size_current_packages'])
        except KeyError:
            pass

        table = json2html.convert(json=repo)
        with open(path.join(repo_dir, filename + '.html'), 'w') as f:
            f.write(template.render(repo=repo, table=table))

    # render repo_index.html
    template = env.get_template('repo_index.html')
    index = template.render(status=status, last_check=last_check,)
    with open(path.join(repo_dir, 'index.html'), 'w') as fp:
        fp.write(index)

    # render index.html
    template = env.get_template('index.html')
    index = template.render(status=status, last_check=last_check,)
    with open(path.join(dir, 'index.html'), 'w') as fp:
        fp.write(index)
